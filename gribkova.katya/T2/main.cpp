#include <iostream>
#include <sstream>
#include <string>
#include <cassert>
#include <iterator>
#include <vector>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <functional>

namespace nspace {
    struct Point {
        int x, y;
    };

    struct Polygon {
        std::vector< Point > points;
    };

    struct DelimiterIO {
        char exp;
    };

    struct IntegerIO {
        int& ref;
    };
    
    struct CommandNameIO {
        std::string &commandName;
    };

    struct ParameterIO {
        std::string &parameter;
    };

    struct Command {
        std::string commandName;
        std::string parameter;
    };

    struct LabelIO {
        std::string exp;
    };

    class iofmtguard
    {
    public:
        iofmtguard(std::basic_ios< char >& s);
        ~iofmtguard();
    private:
        std::basic_ios< char >& s_;
        char fill_;
        std::streamsize precision_;
        std::basic_ios< char >::fmtflags fmt_;
    };

    std::istream& operator>>(std::istream& in, DelimiterIO&& dest);
    std::istream& operator>>(std::istream& in, IntegerIO&& dest);
    std::istream& operator>>(std::istream& in, Polygon& dest);
    std::istream& operator>>(std::istream& in, CommandNameIO&& dest);
    std::istream& operator>>(std::istream& in, ParameterIO&& dest);
    std::istream& operator>>(std::istream& in, LabelIO&& dest);
    std::istream& operator>>(std::istream& in, Command& dest);

    std::ostream& operator<<(std::ostream& out, const Polygon& dest);
    std::ostream& operator<<(std::ostream& out, const Command& dest);

    bool isDigit(int a);
    void commandProcessing(std::vector<Polygon>& data, std::vector<Command>& commands);
    double areaEven(std::vector<Polygon>& data);
    double areaOdd(std::vector<Polygon>& data);
    double areaMean(std::vector<Polygon>& data);
    double areaVertexes(std::vector<Polygon>& data, int vertexesNum);
    double maxArea(std::vector<Polygon>& data);
    double maxVertexes(std::vector<Polygon>& data);
    double minArea(std::vector<Polygon>& data);
    double minVertexes(std::vector<Polygon>& data);
    double countEven(std::vector<Polygon>& data);
    double countOdd(std::vector<Polygon>& data);
    double countVertex(std::vector<Polygon>& data, size_t vertexNum);
    double lessArea(std::vector<Polygon>& data, Polygon p);
    bool oneIntersection(Polygon toCompare, Polygon standardPolygon);
    int intersections(std::vector<Polygon>& data, Polygon p);
}


int main() {
    using nspace::Polygon;
    using nspace::Command;
    std::vector< Polygon > data;
	std::istringstream iss("\
        4 (0;0) (1;0) (1;1) (0;1)\n \
        4 (1; 1) (2; 1) (2; 2) (1; 2)\n \
        3 (1;1) (1;3) (3;3)\n \
        2 (0  1) (1;3) \n \
        5 (0;0) (0;1) (1;2) (2;1) (2;0) \n \
        ");
    while (!iss.eof()) {
        if (!iss) {
            iss.clear();
            iss.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        std::copy
        (
            std::istream_iterator< Polygon >(iss),
            std::istream_iterator< Polygon >(),
            std::back_inserter(data)
        );
    }

    std::cout << "Data:\n";
    std::copy
    (
        std::begin(data),
        std::end(data),
        std::ostream_iterator< Polygon >(std::cout)
    );

    std::vector< Command > commands;
    std::istringstream iss2("\
        AREA EVEN\n \
        AREA ODD\n \
        AREA MEAN\n \
        AREA 4\n \
        MAX AREA\n \
        MAX VERTEXES\n \
        MIN AREA\n \
        MIN VERTEXES\n \
        COUNT EVEN\n \
        COUNT ODD\n \
        COUNT 3\n \
        LESSAREA  3 (1;1) (5;3) (3;3)\n \
        INTERSECTIONS 4 (2;2) (4;2) (4;4) (2;4)\n \
        \
        AREA EV\n \
        MAX MIN\n \
        LESSAREA  3 (1 1) (5;3) (3;3)\n \
        INTERSECTIONS 4 (2;2) (4;2) (4; \n \
        AEA EVEN\n \
        ");
    std::copy
    (
        std::istream_iterator< Command >(iss2),
        std::istream_iterator< Command >(),
        std::back_inserter(commands)
    );

    std::cout << "\nCommands:\n";
    std::copy
    (
        std::begin(commands),
        std::end(commands),
        std::ostream_iterator< Command >(std::cout)
    );

    std::vector<std::string> executionResults;
    using nspace::commandProcessing;

    std::cout << "\nExecution results:\n";
    commandProcessing(data, commands);
  
    std::cout << std::endl;
	return 0;
}

namespace nspace {
    bool isDigit(int a)
    {
        if (a == '0' || a == '1' || a == '2' || a == '3' || a == '4' || a == '5' || a == '6' || a == '7' || a == '8' || a == '9') {
            return true;
        }
        else {
            return false;
        }
    }

    std::istream& operator>>(std::istream& in, DelimiterIO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry) {
            return in;
        }
        char c = '0';
        in >> c;
        if (in && (c != dest.exp)) {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, IntegerIO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        in >> dest.ref;
        return in;
    }

    std::istream& operator>>(std::istream& in, Polygon& dest) {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        Polygon input;
        int quantity = 0;
        in >> quantity;
        if (!in && quantity < 3) {
            in.setstate(std::ios::failbit);
            return in;
        }
        for (int i = 0; i < quantity; i++) {
            Point point;
            in >> DelimiterIO{ '(' };
            in >> IntegerIO{ point.x };
            in >> DelimiterIO{ ';' };
            in >> IntegerIO{ point.y };
            in >> DelimiterIO{ ')' };
            if (!in) {
                in.setstate(std::ios::failbit);
                return in;
            }
            else {
                input.points.push_back(point);
            }
        }
        if (in) {
            dest = input;
        }
        else {
            in.setstate(std::ios::failbit);
        }
        return in;
    }
    std::istream& nspace::operator>>(std::istream& in, CommandNameIO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        std::string commands[6] = {"AREA", "MAX", "MIN", "COUNT", "LESSAREA", "INTERSECTIONS"};
        std::string command = "";
        getline(in, command, ' ');
        bool flag = false;
        for (int i = 0; i < 6; i++) {
            if (command == commands[i]) {
                flag = true;
            }
        }
        if (flag) {
            dest.commandName = command;
        }
        else {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::istream& nspace::operator>>(std::istream& in, ParameterIO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        getline(in, dest.parameter, '\n');
        return in;
    }

    std::istream& nspace::operator>>(std::istream& in, Command& dest) {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        Command input;
        in >> CommandNameIO{ input.commandName };
        in >> ParameterIO{ input.parameter };

        if (input.commandName == "AREA") {
            if ((input.parameter != "EVEN" && input.parameter != "ODD" && input.parameter != "MEAN")) {
                for (size_t i = 0; i < input.parameter.size(); i++) {
                    if (isDigit(input.parameter[i]) == false) {
                        input.commandName = "<INVALID ";
                        input.parameter = "COMMAND>";
                        dest = input;
                        in.clear();
                        return in;
                    }
                }
            }
            else {
                dest = input;
                in.clear();
                return in;
            }
        }
        else if (input.commandName == "MAX" || input.commandName == "MIN") {
            if ((input.parameter != "AREA" && input.parameter != "VERTEXES")) {
                input.commandName = "<INVALID ";
                input.parameter = "COMMAND>";
                dest = input;
                in.clear();
                return in;
            }
        }
        else if (input.commandName == "COUNT") {
            if ((input.parameter != "EVEN" && input.parameter != "ODD")) {
                for (size_t i = 0; i < input.parameter.size(); i++) {
                    if (isDigit(input.parameter[i]) == false) {
                        input.commandName = "<INVALID ";
                        input.parameter = "COMMAND>";
                        dest = input;
                        in.clear();
                        return in;
                    }
                }
            }
            else {
                dest = input;
                in.clear();
                return in;
            }
        }
        else if (input.commandName == "LESSAREA" || input.commandName == "INTERSECTIONS") {
            Polygon p;
            std::istringstream iss(input.parameter + '\0');
            iss >> p;
            if (!iss)
            {
                input.commandName = "<INVALID ";
                input.parameter = "COMMAND>";
                dest = input;
                in.clear();
                return in;
            }
        }
        dest = input;
        in.clear();
        return in;
    }


    std::istream& operator>>(std::istream& in, LabelIO&& dest) {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        int length = dest.exp.size();
        char* buffer = new char[length];
        in.read(buffer, length);
        for (int i = 0; i < length; i++) {
            if (buffer[i] != dest.exp[i]) {
                in.setstate(std::ios::failbit);
            }
        }
        return in;
    }

    std::ostream& nspace::operator<<(std::ostream& out, const Polygon& dest)
    {
        std::ostream::sentry sentry(out);
        if (!sentry) {
            return out;
        }
        out << dest.points.size();
        for (size_t i = 0; i < dest.points.size(); i++) {
            out << " (" << dest.points[i].x << ";" << dest.points[i].y << ")";
        }
        out << "\n";
        return out;
    }

    std::ostream& nspace::operator<<(std::ostream& out, const Command& dest) {
        std::ostream::sentry sentry(out);
        if (!sentry) {
            return out;
        }
        out << dest.commandName << " " << dest.parameter << "\n";
        return out;
    }




    void commandProcessing(std::vector<Polygon>& data, std::vector<Command>& commands) {
        for (size_t i = 0; i < commands.size(); i++) {
            if (commands[i].commandName + commands[i].parameter == "<INVALID COMMAND>") {
                continue;
            }
            else {
                if (commands[i].commandName == "AREA") {
                    std::stringstream stream;
                    if (commands[i].parameter == "EVEN") {
                        std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(areaEven(data));
                        std::cout << ans << std::endl;
                    }
                    else if (commands[i].parameter == "ODD") {
                        std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(areaOdd(data));
                        std::cout << ans << std::endl;
                    }
                    else if (commands[i].parameter == "MEAN") {
                        std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(areaMean(data));
                        std::cout << ans << std::endl;
                    }
                    else {
                        int num = std::stoi(commands[i].parameter);
                        std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(areaVertexes(data, num));
                        std::cout << ans << std::endl;
                    }
                }
                if (commands[i].commandName == "MAX") {
                    if (commands[i].parameter == "AREA") {
                        std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(maxArea(data));
                        std::cout << ans << std::endl;
                    }
                
                    if (commands[i].parameter == "VERTEXES") {
                        std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(int(maxVertexes(data)));
                        std::cout << ans << std::endl;
                    }
                }

                if (commands[i].commandName == "MIN") {
                    if (commands[i].parameter == "AREA") {
                        std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(minArea(data));
                        std::cout << ans << std::endl;
                    }

                    if (commands[i].parameter == "VERTEXES") {
                        std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(int(minVertexes(data)));
                        std::cout << ans << std::endl;
                    }
                }

                if (commands[i].commandName == "COUNT") {
                    if (commands[i].parameter == "EVEN") {
                        std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(int(countEven(data)));
                        std::cout << ans << std::endl;
                    }
                    else if (commands[i].parameter == "ODD") {
                        std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(int(countOdd(data)));
                        std::cout << ans << std::endl;
                    }
                    else {
                        int num = std::stoi(commands[i].parameter);
                        std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(int(countVertex(data, num)));
                        std::cout << ans << std::endl;
                    }
                }
                if (commands[i].commandName == "LESSAREA") {
                    Polygon p;
                    std::istringstream iss(commands[i].parameter);
                    iss >> p;
                    std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(int(lessArea(data, p)));
                    std::cout << ans << std::endl;
                }
                if (commands[i].commandName == "INTERSECTIONS") {
                    Polygon p;
                    std::istringstream iss(commands[i].parameter);
                    iss >> p;
                    std::string ans = commands[i].commandName + " " + commands[i].parameter + "\n" + std::to_string(int(intersections(data, p)));
                    std::cout << ans << std::endl;
                }
            }
        }
    }

    double mult(Point first, Point second) {
        return abs(first.x * second.y - first.y * second.x);
    }

    double oneArea(Polygon polygon) {
        if (polygon.points.size() <= 2) {
            return 0;
        }
        double area = 0.0;
        for (size_t i = 0; i < polygon.points.size(); i++)
        {
            int j = (i + 1) % polygon.points.size();
            area += double(polygon.points[i].x) * double(polygon.points[j].y);
            area -= double(polygon.points[i].y) * double(polygon.points[j].x);
        }
        area = 0.5 * std::abs(area);
        return area;
    }

    double areaEven(std::vector<Polygon>& data) {
        if (data.size() == 0) {
            return 0;
        }
        double result = std::accumulate(data.begin(), data.end(), 0.0,
        [](double res, const Polygon& polygon) {
            if (polygon.points.size() % 2 == 0) {
                res += oneArea(polygon);
            }
            return res;
        }
        );
        return result;
    }

    double areaOdd(std::vector<Polygon>& data) {
        if (data.size() == 0) {
            return 0;
        }
        double result = std::accumulate(data.begin(), data.end(), 0.0,
            [](double res, const Polygon& polygon) {
            if (polygon.points.size() % 2 == 1) {
                res += oneArea(polygon);
            }
            return res;
        }
        );
        return result;
    }

    double areaMean(std::vector<Polygon>& data) {
        if (data.size() == 0) {
            return 0;
        }
        double result = std::accumulate(data.begin(), data.end(), 0.0,
            [](double res, const Polygon& polygon) {
                res += oneArea(polygon);
            return res; } );
        double num = double(data.size());
        return result / num;
    }

    double areaVertexes(std::vector<Polygon>& data, int vertexesNum) {
        if (data.size() == 0) {
            return 0;
        }
        int num = vertexesNum;
        double result = std::accumulate(data.begin(), data.end(), 0.0,
            [vertexesNum](double res, const Polygon& polygon) {
            if (polygon.points.size() == vertexesNum) {
                res += oneArea(polygon);
            }
            return res;
        }
        );
        return result;
    }

    double maxArea(std::vector<Polygon>& data) {
        std::vector<double> areas;
        std::transform(data.begin(), data.end(), back_inserter(areas),
            [](Polygon polygon) {
            return oneArea(polygon);
        });

        double max = *std::max_element(areas.begin(), areas.end());
        return max;
    }

    double maxVertexes(std::vector<Polygon>& data) {
        if (data.size() == 0) {
            return 0;
        }
        std::vector<size_t> vertexes;
        std::transform(data.begin(), data.end(), back_inserter(vertexes),
            [](Polygon polygon) {
            return (polygon.points.size());
        });

        size_t max = *std::max_element(vertexes.begin(), vertexes.end());
        return max;
    }

    double minArea(std::vector<Polygon>& data) {
        if (data.size() == 0) {
            return 0;
        }
        std::vector<double> areas;
        std::transform(data.begin(), data.end(), back_inserter(areas),
            [](Polygon polygon) {
            return oneArea(polygon);
        });

        double min = *std::min_element(areas.begin(), areas.end());
        return min;
    }

    double minVertexes(std::vector<Polygon>& data) {
        if (data.size() == 0) {
            return 0;
        }
        std::vector<size_t> vertexes;
        std::transform(data.begin(), data.end(), back_inserter(vertexes),
            [](Polygon polygon) {
            return (polygon.points.size());
        });

        size_t min = *std::min_element(vertexes.begin(), vertexes.end());
        return min;
    }

    double countEven(std::vector<Polygon>& data) {
        if (data.size() == 0) {
            return 0;
        }
        return std::count_if(data.begin(), data.end(),
            [](Polygon polygon) {
            return (polygon.points.size() % 2 == 0);
        });
    }

    double countOdd(std::vector<Polygon>& data) {
        if (data.size() == 0) {
            return 0;
        }
        return std::count_if(data.begin(), data.end(),
            [](Polygon polygon) {
            return (polygon.points.size() % 2 != 0);
        });
    }

    double countVertex(std::vector<Polygon>& data, size_t vertexNum)
    {
        if (data.size() == 0) {
            return 0;
        }
        return std::count_if(data.begin(), data.end(),
            [vertexNum](Polygon polygon) {
            return (polygon.points.size() == vertexNum);
        });
    }

    double lessArea(std::vector<Polygon>& data, Polygon p) {
        if (data.size() == 0) {
            return 0;
        }
        std::vector<double> areas;
        std::transform(data.begin(), data.end(), back_inserter(areas),
            [](Polygon polygon) {
            return oneArea(polygon);
        });
        double pArea = oneArea(p);
        return std::count_if(areas.begin(), areas.end(),
            [pArea](double area) {
            return (area < pArea);
        });
    }
    
    bool oneIntersection(Polygon toCompare, Polygon standardPolygon) {
        std::vector<int> x;
        std::vector<int> y;

        std::transform(standardPolygon.points.begin(), standardPolygon.points.end(), back_inserter(x),
            [](Point point) {
            return point.x;
        });

        std::transform(standardPolygon.points.begin(), standardPolygon.points.end(), back_inserter(y),
            [](Point point) {
            return point.y;
        });
       double xLeft = double(*std::min_element(x.begin(), x.end()));
       double xRight = double(*std::max_element(x.begin(), x.end()));
       double yHigh = double(*std::max_element(y.begin(), y.end()));
       double yLow = double(*std::min_element(y.begin(), y.end()));

       std::vector<int> xCompare;
       std::vector<int> yCompare;
       std::transform(toCompare.points.begin(), toCompare.points.end(), back_inserter(xCompare),
           [](Point point) {
           return (point.x);
       });
       std::transform(toCompare.points.begin(), toCompare.points.end(), back_inserter(yCompare),
           [](Point point) {
           return (point.y);
       });
       double xLeftComp = double(*std::min_element(xCompare.begin(), xCompare.end()));
       double xRightComp = double(*std::max_element(xCompare.begin(), xCompare.end()));
       double yHighComp = double(*std::max_element(yCompare.begin(), yCompare.end()));
       double yLowComp = double(*std::min_element(yCompare.begin(), yCompare.end()));

       if ((xLeft <= xRightComp && yLow <= yHighComp) || (xRight >= xLeftComp && yLow <= yHighComp)) {
           return true;
       }
       else {
           return false;
       }
    }

    int intersections(std::vector<Polygon>& data, Polygon p) {
        if (data.size() == 0) {
            return 0;
        }
        return std::count_if(data.begin(), data.end(),
            [p](Polygon polygon) {
            return (oneIntersection(polygon, p));
        });
    }

    iofmtguard::iofmtguard(std::basic_ios< char >& s) :
        s_(s),
        fill_(s.fill()),
        precision_(s.precision()),
        fmt_(s.flags())
    {}

    iofmtguard::~iofmtguard()
    {
        s_.fill(fill_);
        s_.precision(precision_);
        s_.flags(fmt_);
    }
}

